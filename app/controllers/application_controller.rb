class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
end

VERSION = '0.0.0b'

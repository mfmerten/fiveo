# README

FiveO Law Enforcement Software Version 3

This application is a complete rewrite of FiveO Version 2 using
Rails 5.0.1 and is not suitable for use at this time.

If you wish to help with the development of this application,
please clone https://bitbucket.org/mfmerten/fiveo-v2 and get
a good idea of the mess its in before volunteering.

My intent is to take v2 features and refactor them for Rails 5 
and Ruby 2.3.  This will be a major task particularly since I began
writing the application when Rails was at 2.3. I was also new to
Ruby and Rails at that time so a lot of the code really smells.

Components of v2 that were intended to help with an upgrade from v1
will be removed in this version. There was only ever 1 company that
used v1 and they've already outgrown it and purchased a commercial
application.

If you still would like to participate, feel free to email me.

Mike Merten
<m.merten@protonmail.com>
